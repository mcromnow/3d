#include "application.h"

#include <iostream>
#include <SDL2/SDL.h>
#include <memory>

#include "gl.h"
#include "gl_program.h"
#include "render_object.h"

namespace {

const float ident[] =
  { 1.0, 0.0, 0.0, 0,
    0.0, 1.0, 0.0, 0,
    0.0, 0.0, 1.0, 0,
    0.0, 0.0, 0.0, 1.0 };

}

Application::Application(int argc, const char* argv[])
  : argc_(argc)
  , argv_(argv)
  , width_(1024)
  , height_(768)
  , running_(true)
  , program_(create_gl_program()) {}

bool Application::init() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    return false;
  }

  window_.reset(SDL_CreateWindow(
      "3D", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
      width_, height_, SDL_WINDOW_OPENGL
  ), SDL_DestroyWindow);

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

  SDL_GL_CreateContext(window_.get());

  setup_opengl();

  std::unique_ptr<RenderObject> o(new RenderObject());
  if (o->load_from("data/cessna.obj"))
    objects.push_back(std::move(o));

  program_->add_shader(GL_VERTEX_SHADER, "shader.vertex");
  program_->add_shader(GL_FRAGMENT_SHADER, "shader.fragment");
  gl_program_ = program_->construct();

  gl_projection_matrix_ = glGetUniformLocation(gl_program_, "Projection");
  gl_world_to_view_matrix_ = glGetUniformLocation(gl_program_, "WorldToView");
  gl_model_to_world_matrix_ = glGetUniformLocation(gl_program_, "ModelToWorld");

  return true;
}

void Application::main_loop() {
  while (running_) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDLK_ESCAPE:
        case SDL_QUIT:
          running_ = false;
      }
    }

    glClear(GL_COLOR_BUFFER_BIT);

    // Clear matrices.
    for (auto i : {gl_projection_matrix_, gl_model_to_world_matrix_,
	  gl_world_to_view_matrix_} ) {
      glUniformMatrix4fv(i, 1, GL_FALSE, ident);
    }

    // TODO: Proper camera transform.
    float s = 0.03;
    float m2w[] = {s  , 0.0, 0.0, 0,
		   0.0, s,   0.0, 0,
		   0.0, 0.0, s,   0,
		   0.0, 0.0, 0.0, 1.0};

    glUniformMatrix4fv(gl_world_to_view_matrix_,
      		       1, GL_FALSE,
		       m2w);
    for (auto &o : objects) {
      // TODO: Model to world tranform.

      o->render();
    }

    SDL_GL_SwapWindow(window_.get());
  }
}

void Application::setup_opengl() {
  //  float ratio = (float) width_ / (float) height_;

  glShadeModel(GL_SMOOTH);

  // glCullFace(GL_BACK);
  // glFrontFace(GL_CCW);
  // glEnable(GL_CULL_FACE);
  // glClearColor(0.5, 0, 0.3, 1.0);
  glViewport(0, 0, width_, height_);
  // glMatrixMode(GL_PROJECTION);
  // glLoadIdentity();
  // gluPerspective(60.0, ratio, 1.0, 1024.0);

}
