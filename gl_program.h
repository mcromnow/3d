#pragma once

#include <memory>
#include "gl.h"

class GLProgram;

std::unique_ptr<GLProgram> create_gl_program();

class GLProgram {
  public:
    virtual ~GLProgram() {}
    virtual bool add_shader(GLuint type, const char* shader_filename) = 0;
    virtual GLuint construct() = 0;
};
