#include "gl_program.h"

#include <assert.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <unordered_map>
#include "gl.h"

class GLProgramImpl : public GLProgram {
  public:
    bool add_shader(GLuint type, const char* shader_filename) {
      assert(installed_shaders_.find(type) == installed_shaders_.end());
      char* shader;
      assert(read_file(shader_filename, &shader));
      installed_shaders_[type] = compile_shader(type, shader);
      delete[] shader;
      return true;
    }

    virtual GLuint construct() {
      gl_program_ = glCreateProgram();
      for (const std::pair<GLuint, GLuint>& shader : installed_shaders_) {
        glAttachShader(gl_program_, shader.second);
      }
      glLinkProgram(gl_program_);
      glUseProgram(gl_program_);
      return gl_program_;
    };

  private:
    bool read_file(const std::string& filename, char** output) const {
      assert(output);
      std::ifstream is(filename);
      if (!is)
        return false;

      // Retrieve the size of the file.
      is.seekg(0, std::ios_base::end);
      int32_t length = is.tellg();
      is.seekg(0, std::ios_base::beg);

      // Read the entire file to |output|.
      *output = new char[length + 1];
      (*output)[length] = '\0';
      is.read(*output, length);

      return true;
    }

    void print_shader_info(GLuint obj) const {
      GLint log_length = 0;
      glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &log_length);

      if (log_length == 0)
        return;

      char* log = new char[log_length];
      glGetShaderInfoLog(obj, log_length, NULL, log);
      std::cerr << log << std::endl;
      delete[] log;
    }

    GLuint compile_shader(GLuint type, const char* shader) const {
      GLuint gl_shader = glCreateShader(type);
      glShaderSource(gl_shader, 1, &shader, NULL);
      glCompileShader(gl_shader);
      print_shader_info(gl_shader);
      return gl_shader;
    }

    std::unordered_map<GLuint, GLuint> installed_shaders_;
    GLuint gl_program_;
};

std::unique_ptr<GLProgram> create_gl_program() {
  return std::unique_ptr<GLProgram>(new GLProgramImpl());
}
