#pragma once

#include <memory>
#include "gl.h"
#include "gl_program.h"
#include "render_object.h"

class RenderObject;
class SDL_Window;

class Application {
  public:
    Application(int argc, const char* argv[]);

    bool init();
    void main_loop();

  private:
    void setup_opengl();

    int argc_;
    const char** argv_;

    std::shared_ptr<SDL_Window> window_;
    int16_t width_;
    int16_t height_;

    bool running_;

    std::unique_ptr<GLProgram> program_;
    GLuint gl_program_;
    GLuint gl_projection_matrix_;
    GLuint gl_world_to_view_matrix_;
    GLuint gl_model_to_world_matrix_;

    std::vector<std::unique_ptr<RenderObject> > objects;
};
