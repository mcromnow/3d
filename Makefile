OS := $(shell uname)
FRAMEWORKS=

ifeq ($(OS), Darwin)
	OS=mac
	FLAGS=-framework OpenGL -framework Cocoa -framework CoreFoundation
	FRAMEWORKS=-framework OpenGL -framework GLUT `sdl2-config --cflags --libs` -lSDL2_ttf
else
	OS=Linux
	FLAGS=-lGL -lGLU `sdl2-config --cflags --libs`
endif

CXXFLAGS=-Wall -Wextra -std=c++11

# OpenGL specific defines
CXXFLAGS+=-DGL_GLEXT_PROTOTYPES

# Setup include path for vmmlib
CXXFLAGS+=-Ithird_party/vmmlib/include/

SOURCES=3d.cc \
	application.cc \
	gl_program_impl.cc \
	render_object.cc \

# Third party sources
SOURCES+= \
	third_party/tinyobjloader/tiny_obj_loader.cc \

TARGET=app

all: $(TARGET)

$(TARGET): $(SOURCES:.cc=.o) $(HEADERS)
	g++ $(CPPFLAGS) $(FRAMEWORKS) -o $(TARGET) -lm -DGL_GLEXT_PROTOTYPES $(SOURCES:.cc=.o) $(FLAGS)

clean:
	rm -f $(TARGET) *.o

