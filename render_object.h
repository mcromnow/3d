#ifndef RENDER_OBJECT_H_
#define RENDER_OBJECT_H_

#include "third_party/tinyobjloader/tiny_obj_loader.h"

#include "gl.h"
#include <string>
#include <vector>

class RenderObject {
public:
  RenderObject();
  ~RenderObject();

  bool load_from(const std::string& filename);

  void create_vertex_array();
  void release_vertex_array();

  void render();

  std::string debug_info() const;

private:
  GLuint vao_;

  tinyobj::attrib_t attrib_;
  std::vector<tinyobj::shape_t> shapes_;
  std::vector<tinyobj::material_t> materials_;
};

#endif
