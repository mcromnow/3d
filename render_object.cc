#include "render_object.h"

#include <iostream>
#include <string>
#include <vector>

RenderObject::RenderObject()
  : vao_(0) {
}

RenderObject::~RenderObject() {
  release_vertex_array();
}

bool RenderObject::load_from(const std::string& filename) {
  std::string err;
  if (!tinyobj::LoadObj(&attrib_, &shapes_, &materials_,
			&err, filename.c_str())) {
    std::cerr << "Failed to load: " << filename << " because: " << err;
    return false;
  }

  // std::cout << err << std::endl; // Warnings...
  // std::cout << debug_info() << std::endl;
  return true;
}

void RenderObject::create_vertex_array() {
  GLuint buffers[3]; // vertices, normals, texcoords

  glGenVertexArrays(1, &vao_);
  glBindVertexArray(vao_);

  glGenBuffers(1 /*3*/, buffers);

  // Vertices
  glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
  glBufferData(GL_ARRAY_BUFFER,
	       sizeof(attrib_.vertices[0]) * attrib_.vertices.size(),
	       &attrib_.vertices[0],
	       GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

#if 0
  // Normals
  glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
  glBufferData(GL_ARRAY_BUFFER,
	       sizeof(attrib_.normals[0]) * attrib_.normals.size(),
	       &attrib_.normals[0],
	       GL_STATIC_DRAW);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

  // Texcoords
  glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
  glBufferData(GL_ARRAY_BUFFER,
	       sizeof(attrib_.texcoords[0]) * attrib_.texcoords.size(),
	       &attrib_.texcoords[0],
	       GL_STATIC_DRAW);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
#endif
  glBindVertexArray(0);
}

void RenderObject::release_vertex_array() {
  glDeleteVertexArrays(1, &vao_);
  vao_ = 0;
}

std::string RenderObject::debug_info() const {
  std::string res = "Object shapes:\n";
  for (auto& shape : shapes_)
    res += " " + shape.name + "\n";
  return res;
}

void RenderObject::render() {
  if (!vao_)
    create_vertex_array();

  glBindVertexArray(vao_);

  std::vector<unsigned int> vertex_indices;
  for (const tinyobj::shape_t& shape : shapes_) {
    for (const tinyobj::index_t& i : shape.mesh.indices)
      vertex_indices.push_back(i.vertex_index);
  }

  glDrawElementsBaseVertex(GL_TRIANGLES,
			   vertex_indices.size(),
			   GL_UNSIGNED_INT,
			   (void*)(&vertex_indices[0]),
			   0);
  glBindVertexArray(0);
}
