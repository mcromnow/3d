#include "application.h"

#include <iostream>

int main(int argc, const char* argv[]) {
  std::cout << "3d world" << std::endl;

  Application application(argc, argv);
  if (application.init()) {
    application.main_loop();
  } else {
    std::cerr << "Failed to initialize " << argv[0] << std::endl;
  }
}
