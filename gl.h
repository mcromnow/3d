#pragma once

#if defined(__linux__)
# include <GL/gl.h>
# include <GL/glu.h>
#elif __APPLE__
# include <OpenGL/gl3.h>
# include <OpenGL/glu.h>
#endif


